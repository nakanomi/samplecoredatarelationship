//
//  BlogEntry.m
//  sampleCoreDataRelationship
//
//  Created by  on 12/07/17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BlogEntry.h"
#import "BlogComment.h"


@implementation BlogEntry

@dynamic content;
@dynamic created;
@dynamic title;
@dynamic comments;
@dynamic tags;

@end
