//
//  Tag.m
//  sampleCoreDataRelationship
//
//  Created by  on 12/07/17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "Tag.h"
#import "BlogEntry.h"


@implementation Tag

@dynamic name;
@dynamic entries;

@end
