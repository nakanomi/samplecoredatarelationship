//
//  BlogEntry.h
//  sampleCoreDataRelationship
//
//  Created by  on 12/07/17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BlogComment;

@interface BlogEntry : NSManagedObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *comments;
@property (nonatomic, retain) NSSet *tags;
@end

@interface BlogEntry (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(BlogComment *)value;
- (void)removeCommentsObject:(BlogComment *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;
- (void)addTagsObject:(NSManagedObject *)value;
- (void)removeTagsObject:(NSManagedObject *)value;
- (void)addTags:(NSSet *)values;
- (void)removeTags:(NSSet *)values;
@end
