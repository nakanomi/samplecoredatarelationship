//
//  AppDelegate.m
//  sampleCoreDataRelationship
//
//  Created by  on 12/07/17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

#import "BlogEntry.h"
#import "BlogComment.h"
#import "Tag.h"

#import "MasterViewController.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize navigationController = _navigationController;

- (void)dealloc
{
	[_window release];
	[__managedObjectContext release];
	[__managedObjectModel release];
	[__persistentStoreCoordinator release];
	[_navigationController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	{
		if ([self addTestData]) {
			NSLog(@"test data registered");
			[self addComment];
		}
		else {
			NSLog(@"test data is already registered");
		}
		[self fetchByBlogEntry];
		[self fetchByTag];
	}
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.

	MasterViewController *masterViewController = [[[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil] autorelease];
	self.navigationController = [[[UINavigationController alloc] initWithRootViewController:masterViewController] autorelease];
	masterViewController.managedObjectContext = self.managedObjectContext;
	self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Saves changes in the application's managed object context before the application terminates.
	[self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"sampleCoreDataRelationship" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"sampleCoreDataRelationship.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark -実験用の追加コード開始

#pragma mark -データ追加
//実験要データを追加する
- (BOOL)addTestData
{
	if ([self getCount]) {
		return NO;
	}
	
	NSManagedObjectContext* moc = [self managedObjectContext];
	NSError* error = nil;
	
	Tag* tag1 = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
											  inManagedObjectContext:moc];
	tag1.name = @"Mac";
	
	Tag* tag2 = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
											  inManagedObjectContext:moc];
	tag2.name = @"iPhone";
	
	Tag* tag3 = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
											  inManagedObjectContext:moc];
	tag3.name = @"iPad";
	
	[moc save:&error];
	if (error) {
		NSLog(@"INSERT ERROR: %@", error);
	} else {
		NSLog(@"INSERTED: Tag");
	}	
	
	
	BlogEntry* blog1 = [NSEntityDescription insertNewObjectForEntityForName:@"BlogEntry"
													 inManagedObjectContext:moc];
	blog1.title = @"CoreData のリレーションシップについて";
	blog1.content = @"かくかくしかじか";
	blog1.created = [NSDate date];
	//	[blog1 addTags:[NSSet setWithObjects:tag1, tag2, tag3, nil]];
	blog1.tags = [NSSet setWithObjects:tag1, tag2, tag3, nil];
	
	BlogEntry* blog2 = [NSEntityDescription insertNewObjectForEntityForName:@"BlogEntry"
													 inManagedObjectContext:moc];
	blog2.title = @"iPad 5/28発売";
	blog2.content = @"かくかくしかじか";
	blog2.created = [NSDate date];
	blog2.tags = [NSSet setWithObjects:tag3, nil];
	//	[blog2 addTags:[NSSet setWithObjects:tag3, nil]];
	
	[moc save:&error];
	if (error) {
		NSLog(@"INSERT ERROR: %@", error);
	} else {
		NSLog(@"INSERTED: BlogEntry");
	}
	return YES;
}

-(void)addComment
{
	NSManagedObjectContext* moc = [self managedObjectContext];
	
	// (1) fetch from BlogEntry
	NSFetchRequest* request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:@"BlogEntry"
								   inManagedObjectContext:self.managedObjectContext]];
	
	NSError* error = nil;
	NSArray* entries = [moc executeFetchRequest:request error:&error];
	
	[request release];
	
	// (2) add comments	
	BlogComment* comment1 = [NSEntityDescription insertNewObjectForEntityForName:@"BlogComment"
														  inManagedObjectContext:moc];
	comment1.comment = @"RDBとはモデリング手法が若干異なる。";
	comment1.created = [NSDate date];
	
	BlogComment* comment2 = [NSEntityDescription insertNewObjectForEntityForName:@"BlogComment"
														  inManagedObjectContext:moc];
	comment2.comment = @"徐々にイメージがわいてきた";
	comment2.created = [NSDate date];
	
	BlogEntry* entry1 = [entries objectAtIndex:0];
	[entry1 addCommentsObject:comment1];
	[entry1 addCommentsObject:comment2];
	
	
	BlogComment* comment3 = [NSEntityDescription insertNewObjectForEntityForName:@"BlogComment"
														  inManagedObjectContext:moc];
	comment3.comment = @"楽しみ";
	comment3.created = [NSDate date];
	BlogEntry* entry2 = [entries objectAtIndex:1];
	[entry2 addCommentsObject:comment3];
	
	
	[moc save:&error];
	if (error) {
		NSLog(@"ADD ERROR: %@", error);
	} else {
		NSLog(@"ADDED: BlogEntry");
	}	
}

#pragma mark -フェッチ
//ブログエントリをフェッチする
- (void)fetchByBlogEntry
{
	NSLog(@"----- %s ---------------------------------------------------", __PRETTY_FUNCTION__);
	NSManagedObjectContext* moc = [self managedObjectContext];
	
	NSFetchRequest* request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:@"BlogEntry"
								   inManagedObjectContext:self.managedObjectContext]];
	
	NSError* error = nil;
	NSLog(@"----- executeFetchRequest ------------------------------------------");
	NSArray* list = [moc executeFetchRequest:request error:&error];
	
	NSLog(@"----- listup entries ---------------------------------------------------");
	for (BlogEntry* entry in list) {
		NSLog(@"title=%@", entry.title);
		for (Tag* tag in entry.tags) {
			NSLog(@"    *tag*: %@", tag.name);
		}
		for (BlogComment* comment in entry.comments) {
			NSLog(@"    *comment*: %@",comment.comment);
		}
	}
	
	[request release];
	
}
//タグをフェッチする
- (void)fetchByTag
{
	NSLog(@"----- %s ---------------------------------------------------", __PRETTY_FUNCTION__);
	NSManagedObjectContext* moc = [self managedObjectContext];
	
	NSFetchRequest* request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:@"Tag"
								   inManagedObjectContext:self.managedObjectContext]];
	NSError* error = nil;
	NSLog(@"----- executeFetchRequest ------------------------------------------");
	NSArray* list = [moc executeFetchRequest:request error:&error];
	
	NSLog(@"----- listup entries ---------------------------------------------------");
	
	for (Tag *tag in list) {
		NSLog(@"tagname : %@", tag.name);
		for (BlogEntry *entry in tag.entries)
		{
			NSLog(@"    entry title : %@", entry.title);
		}
	}
}




- (NSUInteger)getCount
{
	NSManagedObjectContext* moc = [self managedObjectContext];
	
	NSFetchRequest* request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:@"BlogEntry"
								   inManagedObjectContext:self.managedObjectContext]];
	[request setIncludesSubentities:NO];
	
	NSError* error = nil;
	NSUInteger count = [moc countForFetchRequest:request error:&error];
	if (count == NSNotFound) {
		count = 0;
	}
	[request release];
	
	return count;
}


@end
